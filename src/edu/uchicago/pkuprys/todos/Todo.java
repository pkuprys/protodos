package edu.uchicago.pkuprys.todos;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.UUID;

//model for todos
public class Todo {

    private static final String JSON_ID = "id";
    private static final String JSON_TITLE = "title";
    private static final String JSON_DETAILS = "details";
    private static final String JSON_DATE = "date";
    private static final String JSON_DONE = "done";

    private UUID mId;
    private String mTitle;
    private String mDetails;
    private Date mDate;
    private boolean mDone;

    public Todo() {
        mId = UUID.randomUUID();
        mDate = new Date();
    }

    public Todo(JSONObject json) throws JSONException {
        mId = UUID.fromString(json.getString(JSON_ID));
        mTitle = json.getString(JSON_TITLE);
        mDetails = json.getString(JSON_DETAILS);
        mDate = new Date(json.getLong(JSON_DATE));
        mDone = json.getBoolean(JSON_DONE);
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, mId.toString());
        json.put(JSON_TITLE, mTitle);
        json.put(JSON_DETAILS, mDetails);
        json.put(JSON_DATE, mDate.getTime());
        json.put(JSON_DONE, mDone);
        return json;
    }

    @Override
    public String toString() {
        return mTitle;
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDetails() {
        return mDetails;
    }

    public void setDetails(String details) {
        mDetails = details;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public boolean isDone() {
        return mDone;
    }

    public void setDone(boolean done) {
        mDone = done;
    }
}
