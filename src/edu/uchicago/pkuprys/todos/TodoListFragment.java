package edu.uchicago.pkuprys.todos;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class TodoListFragment extends ListFragment {
    private ArrayList<Todo> mTodos;
    private Callbacks mCallbacks;

    public interface Callbacks {
        void onTodoSelected(Todo todo);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public void updateUI() {
        ((TodoAdapter) getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.todos_title);
        mTodos = TodoRepository.get(getActivity()).getTodos();
        TodoAdapter adapter = new TodoAdapter(mTodos);
        setListAdapter(adapter);
        setRetainInstance(true);
    }

    @TargetApi(11)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, parent, savedInstanceState);

        ListView listView = (ListView) v.findViewById(android.R.id.list);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            registerForContextMenu(listView);
        } else {
            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            listView.setMultiChoiceModeListener(new MultiChoiceModeListener() {

                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.todo_list_item_context, menu);
                    return true;
                }

                public void onItemCheckedStateChanged(ActionMode mode, int position,
                                                      long id, boolean checked) {
                }

                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_item_delete_todo:
                            TodoAdapter adapter = (TodoAdapter) getListAdapter();
                            TodoRepository todoLab = TodoRepository.get(getActivity());
                            for (int i = adapter.getCount() - 1; i >= 0; i--) {
                                if (getListView().isItemChecked(i)) {
                                    todoLab.deleteTodo(adapter.getItem(i));
                                }
                            }
                            mode.finish();
                            adapter.notifyDataSetChanged();
                            return true;
                        default:
                            return false;
                    }
                }

                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                public void onDestroyActionMode(ActionMode mode) {

                }
            });
        }
        return v;
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        // get the Tod from the adapter
        Todo t = ((TodoAdapter) getListAdapter()).getItem(position);
        mCallbacks.onTodoSelected(t);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ((TodoAdapter) getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_todo_list, menu);
    }

    @TargetApi(11)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_todo:
                Todo todo = new Todo();
                TodoRepository.get(getActivity()).addTodo(todo);
                ((TodoAdapter) getListAdapter()).notifyDataSetChanged();
                mCallbacks.onTodoSelected(todo);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        getActivity().getMenuInflater().inflate(R.menu.todo_list_item_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        int position = info.position;
        TodoAdapter adapter = (TodoAdapter) getListAdapter();
        Todo todo = adapter.getItem(position);

        switch (item.getItemId()) {
            case R.id.menu_item_delete_todo:
                TodoRepository.get(getActivity()).deleteTodo(todo);
                adapter.notifyDataSetChanged();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private class TodoAdapter extends ArrayAdapter<Todo> {
        public TodoAdapter(ArrayList<Todo> todos) {
            super(getActivity(), android.R.layout.simple_list_item_1, todos);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // if we weren't given a view, inflate one
            if (null == convertView) {
                convertView = getActivity().getLayoutInflater()
                        .inflate(R.layout.list_item_todo, null);
            }

            // configure the view for this Todo
            Todo t = getItem(position);

            TextView titleTextView =
                    (TextView) convertView.findViewById(R.id.todo_list_item_titleTextView);
            titleTextView.setText(t.getTitle());

            CheckBox doneCheckBox =
                    (CheckBox) convertView.findViewById(R.id.todo_list_item_solvedCheckBox);
            doneCheckBox.setChecked(t.isDone());

            return convertView;
        }
    }
}
