package edu.uchicago.pkuprys.todos;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class TodoListActivity extends SingleFragmentActivity
        implements TodoListFragment.Callbacks, TodoFragment.Callbacks {

    @Override
    protected Fragment createFragment() {
        return new TodoListFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    public void onTodoSelected(Todo todo) {
        if (findViewById(R.id.detailFragmentContainer) == null) {
            // start an instance of TodoPagerActivity
            Intent i = new Intent(this, TodoPagerActivity.class);
            i.putExtra(TodoFragment.EXTRA_TODO_ID, todo.getId());
            startActivityForResult(i, 0);
        } else {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();

            Fragment oldDetail = fm.findFragmentById(R.id.detailFragmentContainer);
            Fragment newDetail = TodoFragment.newInstance(todo.getId());

            if (oldDetail != null) {
                ft.remove(oldDetail);
            }
            ft.add(R.id.detailFragmentContainer, newDetail);
            ft.commit();
        }
    }

    public void onTodoUpdated(Todo todo) {
        FragmentManager fm = getSupportFragmentManager();
        TodoListFragment listFragment = (TodoListFragment)
                fm.findFragmentById(R.id.fragmentContainer);
        listFragment.updateUI();
    }
}
