package edu.uchicago.pkuprys.todos;

import android.support.v4.app.Fragment;

import java.util.UUID;

public class TodoActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        UUID todoId = (UUID) getIntent()
                .getSerializableExtra(TodoFragment.EXTRA_TODO_ID);
        return TodoFragment.newInstance(todoId);
    }
}
