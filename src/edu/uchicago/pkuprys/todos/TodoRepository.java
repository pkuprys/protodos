package edu.uchicago.pkuprys.todos;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

public class TodoRepository {
    private static final String TAG = "TodoRepository";
    private static final String FILENAME = "todos.json";

    private ArrayList<Todo> mTodos;
    private TodosJSONSerializer mSerializer;

    private static TodoRepository sTodoLab;
    private Context mAppContext;

    private TodoRepository(Context appContext) {
        mAppContext = appContext;
        mSerializer = new TodosJSONSerializer(mAppContext, FILENAME);

        try {
            mTodos = mSerializer.loadTodos();
        } catch (Exception e) {
            mTodos = new ArrayList<Todo>();
            Log.e(TAG, "Error loading todos: ", e);
        }
    }

    public static TodoRepository get(Context c) {
        if (sTodoLab == null) {
            sTodoLab = new TodoRepository(c.getApplicationContext());
        }
        return sTodoLab;
    }

    public Todo getTodo(UUID id) {
        for (Todo t : mTodos) {
            if (t.getId().equals(id))
                return t;
        }
        return null;
    }

    public void addTodo(Todo t) {
        mTodos.add(t);
        saveTodos();
    }

    public ArrayList<Todo> getTodos() {
        return mTodos;
    }

    public void deleteTodo(Todo t) {
        mTodos.remove(t);
        saveTodos();
    }

    public boolean saveTodos() {
        try {
            mSerializer.saveTodos(mTodos);
            Log.d(TAG, "todos saved to file");
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Error saving todos: " + e);
            return false;
        }
    }
}

