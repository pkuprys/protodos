package edu.uchicago.pkuprys.todos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.UUID;

public class TodoPagerActivity extends FragmentActivity implements TodoFragment.Callbacks {
    ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewPager = new ViewPager(this);
        mViewPager.setId(R.id.viewPager);
        setContentView(mViewPager);

        final ArrayList<Todo> todos = TodoRepository.get(this).getTodos();

        FragmentManager fm = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
            @Override
            public int getCount() {
                return todos.size();
            }

            @Override
            public Fragment getItem(int pos) {
                UUID todoId = todos.get(pos).getId();
                return TodoFragment.newInstance(todoId);
            }
        });

        UUID todoId = (UUID) getIntent().getSerializableExtra(TodoFragment.EXTRA_TODO_ID);
        for (int i = 0; i < todos.size(); i++) {
            if (todos.get(i).getId().equals(todoId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }

    public void onTodoUpdated(Todo todo) {
        // do nothing        
    }
}
